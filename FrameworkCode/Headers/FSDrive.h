/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef FSDrive_H
#define FSDrive_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    

// typedefs for the states
// State definitions for use with the query function
typedef enum { InitPState, RunningState, PauseState} FSDriveState_t ;

// Public Function Prototypes

bool InitFSDriveService ( uint8_t Priority );
bool PostFSDriveService( ES_Event_t ThisEvent );
ES_Event_t RunFSDriveService( ES_Event_t ThisEvent );
bool CheckSwitchPress(void);

#endif /* FSDrive_H */


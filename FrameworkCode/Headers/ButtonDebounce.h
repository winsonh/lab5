/****************************************************************************
 
  Header file for Test Harness Service0 
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef Button_Debounce_Service_H
#define Button_Debounce_Service_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Events.h"    

// typedefs for the states
// State definitions for use with the query function
typedef enum { Debouncing,
               Ready2Sample,
							} ButtonDebounceState_t ;

// Public Function Prototypes
bool InitializeButtonDebounce(uint8_t Priority);
bool PostButtonDebounceService( ES_Event_t ThisEvent );
ES_Event_t RunButtonDebounceSM(ES_Event_t ThisEvent);
bool CheckButtonEvents(void);
              
#endif /* Button_Debounce_Service_H */

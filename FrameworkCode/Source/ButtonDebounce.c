/****************************************************************************
 Module
   MorseElementsService.c

Pseudo-code for the Morse Elements module (a service that implements a state 
machine)Data private to the module: MyPriority, CurrentState, TimeOfLastRise, 
TimeOfLastFall, LengthOfDot, FirstDelta, LastInputState
****************************************************************************/
// #define TEST

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <termio.h>

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"


// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

#include "BITDEFS.H"
#include "ButtonDebounce.h"


// readability defines
#define ALL_BITS (0xff << 2)



/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/



/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t CurrentState;
static uint8_t LastButtonState;
static uint8_t MyPriority;

/*------------------------------ Module Code ------------------------------*/

/*
*/
bool InitializeButtonDebounce(uint8_t Priority) {
  bool ReturnVal = false;
  MyPriority = Priority;
  
  /*
	Initialize the port line to monitor the button
	*/
  // Port Initialization
  /*
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R1; // Enable Port B
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R1) != SYSCTL_PRGPIO_R1){
  }
  */
 
  // Digital Port Selection
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= BIT4HI; // Make bit 3 of Port B to be digital
  
  // Data Direction Setting
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) &= BIT4LO; // Make bit 3 of Port B to be intput
  
  // Set bit 4 low.
  HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) &= BIT4LO; // |=  Make bit 3 of Port B to be low
	
	// Sample port line and use it to initialize the LastInputState variable
	LastButtonState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT4HI;
	
	// Set CurrentState to be Debouncing
  CurrentState = Debouncing;
  
  // Start debounce timer (Timer posts to ButtonDebounceSM)
  ES_Timer_InitTimer(Debounce_TIMER,50);
  
  ReturnVal = true;
  return ReturnVal;
}


/*
*/
bool CheckButtonEvents(void) {
  // printf("CheckButtonEvents\n");
  
  bool ReturnVal = false;
  uint8_t CurrentButtonState;
  
  CurrentButtonState = HWREG(GPIO_PORTB_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT4HI;
  
  if(CurrentButtonState != LastButtonState) {
    ReturnVal = true;
    
    if(CurrentButtonState == 0) {
      // printf("Button Released\n");
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ButtonDown;
      PostButtonDebounceService(ThisEvent);
    }
    else {
      // printf("Button Pressed\n");
      ES_Event_t ThisEvent;
      ThisEvent.EventType = ButtonUp;
      PostButtonDebounceService(ThisEvent);
    }
  }
  LastButtonState = CurrentButtonState;
  return ReturnVal;
}



/*
*/
ES_Event_t RunButtonDebounceSM(ES_Event_t ThisEvent) {
  struct ES_Event ReturnEvent;

  if(CurrentState == Debouncing) {
    // printf("Debouncing State\n");
    if( (ThisEvent.EventType == ES_TIMEOUT) && (ThisEvent.EventParam == Debounce_TIMER) ) {
      // printf("CHANGE to Read2Sample");
      CurrentState = Ready2Sample;
    }
  }
  
  else if(CurrentState == Ready2Sample) {
     // printf("Ready2Sample State\n");
     if(ThisEvent.EventType == ButtonUp ) {
       ES_Timer_InitTimer(Debounce_TIMER,50);
       CurrentState = Debouncing;
       
       ES_Event_t ThisEvent;
       ThisEvent.EventType = DBButtonUp;
       PostButtonDebounceService(ThisEvent);
       PostMorseElementService(ThisEvent);
     }
     
     if(ThisEvent.EventType == ButtonDown) {
       ES_Timer_InitTimer(Debounce_TIMER,50);
       CurrentState = Debouncing;
       
       ES_Event_t ThisEvent;
       ThisEvent.EventType = DBButtonDown;
       PostButtonDebounceService(ThisEvent);
       PostMorseElementService(ThisEvent);
     }
  }
  
  
  ReturnEvent.EventType = ES_NO_EVENT;
  return ReturnEvent;
}




/*
*/
bool PostButtonDebounceService( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}












#ifdef TEST


int main(void) {
  TERMIO_Init();
  printf("TEST NOW!\n");
  InitializeButtonDebounce(8);
  
	
  while(!kbhit()) {
    CheckButtonEvents();
  }
  


}
#endif

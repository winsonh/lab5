/****************************************************************************
 Module
   FSDrive.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the 
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/25/17 13:56 jec      added comments about where to init deferal que and
                         fixed bad ONE_SEC definition left over from HC12
 10/19/16 13:24 jec      added comments about where to add deferal and recall
 01/12/15 21:47 jec      converted to LCD module for lab 3
 11/02/13 17:21 jec      added exercise of the event deferral/recall module
 08/05/13 20:33 jec      converted to test harness service
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
// #define TEST
/*----------------------------- Include Files -----------------------------*/
/* include header files for this service
*/
#include "FSDrive.h"

/* include header files for hardware access
*/
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "ADMulti.h"
#include "PWM16Tiva.h"

/* include header files for the framework
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"

/* include header files for the other modules in Lab3 that are referenced
*/
// #include "LCD_Write.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 1.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC/2)
#define TWO_SEC (ONE_SEC*2)
#define FIVE_SEC (ONE_SEC*5)
#define NUM_PWM_PINS 16
#define PE0 0                         /*The array index of AD Input Pin*/
#define PF0 12
#define PF1 13
#define PF2 14
#define PF3 15
#define NUM_STEPS 4
#define NUM_VOLTAGE_OUTPUT_PER_STEP 4
#define NUM_AD_INPUT_PORTS 4
#define SWITCH_OPEN 16
#define SWITCH_CLOSE 0
#define MAX_MOVES_NUM 99999999 //200
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static uint16_t GetStepInterval();


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint16_t MoveNum = 0;
static int8_t MotorDirection = 1;
static int8_t CurrentStep = 0;
static uint8_t MyPriority;
static FSDriveState_t CurrentState = InitPState;
static uint8_t Last_PF4_Status = 16;
static const uint8_t FS_DriveArray[16] = {100, 0, 100, 0,
                                         0, 100, 100, 0, 
                                         0, 100, 0, 100,
                                         100, 0, 0, 100};

static const uint8_t WD_DriveArray[16] = {100, 0, 0, 0,
                                         0, 0, 100, 0, 
                                         0, 100, 0, 0,
                                         0, 0, 0, 100};

static const uint8_t HS_DriveArray[32] = {100, 0, 0, 0,
                                          100, 0, 100, 0,
                                          0, 0, 100, 0,
                                          0, 100, 100, 0,
                                          0, 100, 0, 0,
                                          0, 100, 0, 100,
                                          0, 0, 0, 100,
                                          100, 0, 0, 100};
                                         

static const uint8_t MS_DriveArray[64] = {71, 0, 71, 0,
                                         92, 0, 38, 0, 
                                         100, 0, 0, 0,
                                         92, 0, 0, 38,
                                         71, 0, 0, 71,
                                         38, 0, 0, 92,
                                         0, 0, 0, 100,
                                         0, 38, 0, 92,
                                         0, 71, 0, 71,
                                         0, 92, 0, 38, 
                                         0, 100, 0, 0,
                                         0, 92, 38, 0,
                                         0, 71, 71, 0,
                                         0, 38, 92, 0,
                                         0, 0, 100, 0,
                                         38, 0, 92, 0};



static uint32_t ADResults[NUM_AD_INPUT_PORTS];
// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event_t DeferralQueue[3+1];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitFSDriveService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any 
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitFSDriveService ( uint8_t Priority )
{
  ES_Event_t ThisEvent;
  
  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  
  // Initialize PF0, PF1, PF2, PF3 as PWM outputs
  PWM_TIVA_Init(NUM_PWM_PINS);
  
  // Initialize PE0 as AD Converter input pin
  ADC_MultiInit(4); 
  
  // Initialize Port F
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R5; // Enable Port F
  while ( (HWREG(SYSCTL_PRGPIO) & SYSCTL_PRGPIO_R5) != SYSCTL_PRGPIO_R5){;}
  // Set PF4 to be digital
  HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= (BIT4HI);
  // Set PF4 to be input
  HWREG(GPIO_PORTF_BASE + GPIO_O_DIR) &= (BIT4LO);
  // Activate the internal pull-up resistor of PF4
  HWREG(GPIO_PORTF_BASE + GPIO_O_PUR) |= (BIT4HI); 
  
  
  //Put us into the initial pseudo-state to set up for the initial transition
	CurrentState = InitPState;

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService( MyPriority, ThisEvent) == true)
  {  
    return true;
  }else
  {
      return false;
  }
}

/****************************************************************************
 Function
     PostLCDService

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostFSDriveService( ES_Event_t ThisEvent )
{
  return ES_PostToService( MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunLCDService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/

ES_Event_t RunFSDriveService( ES_Event_t ThisEvent )
{
  struct ES_Event ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  
  
  switch (CurrentState){
    case InitPState :
      if ( ThisEvent.EventType == ES_INIT ){
        CurrentState = RunningState;
        
        uint8_t StepInterval = GetStepInterval();
        // printf("\n\rStepInterval = %d", StepInterval);
        
        ES_Timer_StopTimer(FSD_STEP_TIMER);
        ES_Timer_SetTimer(FSD_STEP_TIMER, 1000);
        // ES_Timer_SetTimer(FSD_STEP_TIMER, 2);
        ES_Timer_StartTimer(FSD_STEP_TIMER);
      }
      break;
      
      
    case RunningState :
      if( ThisEvent.EventType == ES_TIMEOUT ){
        
        if(CurrentStep > 63) { // 31) { //15){ //63) {
          CurrentStep = 0;
        }

        if(CurrentStep < 0) {
          CurrentStep = 60; // 28; // 12; //60;
        }
        
        PWM_TIVA_SetDuty(MS_DriveArray[CurrentStep] , PF0);
        // printf("\n\r%d", MS_DriveArray[CurrentStep]);
        PWM_TIVA_SetDuty(MS_DriveArray[CurrentStep + 1] , PF1);
        // printf("\n\r%d", MS_DriveArray[CurrentStep + 1]);
        PWM_TIVA_SetDuty(MS_DriveArray[CurrentStep + 2] , PF2);
        // printf("\n\r%d", MS_DriveArray[CurrentStep + 2]);
        PWM_TIVA_SetDuty(MS_DriveArray[CurrentStep + 3] , PF3);
        // printf("\n\r%d\n", MS_DriveArray[CurrentStep + 3]);
        
        MoveNum = MoveNum + 1;
        // printf("\n\rMoveNum = %d", MoveNum);
        
        if(MoveNum == MAX_MOVES_NUM) {
          // MotorDirection = MotorDirection * (-1);
        }
        
        if(MoveNum < (2*MAX_MOVES_NUM)) {
          uint8_t StepInterval = GetStepInterval();
          // printf("\n\rStepInterval = %d", StepInterval);
          
          ES_Timer_StopTimer(FSD_STEP_TIMER);
          ES_Timer_SetTimer(FSD_STEP_TIMER, 1000);
          // ES_Timer_SetTimer(FSD_STEP_TIMER, 2);
          ES_Timer_StartTimer(FSD_STEP_TIMER);
          
          CurrentStep = CurrentStep + (MotorDirection*4);
        }

      }
      
      if( ThisEvent.EventType == ES_CHANGE_DIRECTION ) {
        MotorDirection = MotorDirection * (-1);
        // printf("\n\rMotorDirection = %d", MotorDirection);
      }
  }
  
  return ReturnEvent;
}








bool CheckSwitchPress(void) {

  bool ReturnVal = false;
  uint16_t CurrentADCounts = 0;
  
  uint8_t PF4_status = (HWREG(GPIO_PORTF_BASE + (GPIO_O_DATA + ALL_BITS)) & BIT4HI); 
  
  if(PF4_status != Last_PF4_Status) {
    
    if(PF4_status == SWITCH_OPEN) {
      // printf("\n\rButton Released");
      
      ES_Event_t SwitchEvent;
      SwitchEvent.EventType = ES_CHANGE_DIRECTION;
      PostFSDriveService(SwitchEvent);
      
      ReturnVal = true;
    }
    
    if(PF4_status == SWITCH_CLOSE) {
      // printf("\n\rSwitch Pressed");
    }
    
    Last_PF4_Status = PF4_status;
  }
  
  return ReturnVal;
}







/***************************************************************************
 private functions
 ***************************************************************************/



static uint16_t GetStepInterval() {
  uint16_t StepFrequency = 0;
  ADC_MultiRead(ADResults);
  StepFrequency = (390.0/4095.0) * ADResults[PE0] + 10;
  uint16_t StepInterval = ONE_SEC / StepFrequency;
  
  return StepInterval;
  
}















#ifdef TEST
#include <termio.h>

int main(void) {
  TERMIO_Init();
  printf("TEST NOW!\n");
  InitFSDriveService(MyPriority);
  
  /*
  int i = 0;
  while(!kbhit()) {
    for(int j = 0; j < 100000; j++) {
      ;
    }
    
    if(i >= 16) {
      i = 0;
    }
    
    PWM_TIVA_SetDuty(FS_StepArray[i] , PF0);
    // printf("\n\r%d", FS_StepArray[i]);
    PWM_TIVA_SetDuty(FS_StepArray[i+1] , PF1);
    // printf("\n\r%d", FS_StepArray[i+1]);
    PWM_TIVA_SetDuty(FS_StepArray[i+2] , PF2);
    // printf("\n\r%d", FS_StepArray[i+2]);
    PWM_TIVA_SetDuty(FS_StepArray[i+3] , PF3);
    // printf("\n\r%d\n", FS_StepArray[i+3]);
    
    i = i + 4;
    
    ADC_MultiRead(ADResults);
    printf("\r\nCh0 = %u, Ch1 = %u, Ch2 = %u, Ch3 = %u", ADResults[0], ADResults[1], ADResults[2], ADResults[3]);

  }
  */

  
  while(!kbhit()) {
   for(int j = 0; j < 100000; j++) {
      ;
   }
   CheckSwitchPress();
   printf("\n\rStepInterval = %d", GetStepInterval());
  }
  
  
  
  return 0;
}

#endif

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

